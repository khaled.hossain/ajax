package edu.missouristate.model;

import java.util.ArrayList;
import java.util.List;

public class RequestResponse {
	String firstName; // send data to the server
	String lastName;  // send data to the server
	String errorMessage = "false"; // back to the caller (fetch)
	String successMessage = "";    // back to the caller (fetch)
	List<User> list = new ArrayList<User>(); // back to the caller (fetch)

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<User> getList() {
		return list;
	}

	public void setList(List<User> list) {
		this.list = list;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

}
